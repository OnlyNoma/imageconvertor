<?php

ini_set('max_execution_time', 300);

class JpgProgressiveConverter
{
    /**
     * getExt
     *
     * @param $image
     *
     * @return string
     */
    public function getExt($image)
    {
        $exploded = explode('.', $image);
        $ext = $exploded[sizeof($exploded) - 1];

        return $ext;
    }

    /**
     * getName
     *
     * @param $image
     *
     * @return string
     */
    public function getName($image)
    {
        $exploded = explode('.', $image);
        $name = $exploded[sizeof($exploded) - 2];

        return $name;
    }

    /**
     * isProgressive
     *
     * @param $filepath
     * @return bool
     */
    function isProgressive($filepath)
    {
        $result = false;
        if (file_exists($filepath)) {
            $fs = @fopen($filepath, "rb");
            $byte_last = 0;
            $buffer_length = 4 * 1024;
            $begins_with_SOI = false;
            while ($buffer = fread($fs, $buffer_length)) {
                if ($byte_last) {
                    $buffer = $byte_last . $buffer;
                }
                $byte_last = 0;
                preg_match("/\.$/", $buffer, $matches);
                if (count($matches)) {
                    $byte_last = $matches[0];
                }
                if (!$begins_with_SOI) {
                    preg_match("/^\\xff\\xd8/", $buffer, $matches);
                    if (count($matches)) {
                        $begins_with_SOI = true;
                    } else {
                        $result = false;
                        break;
                    }
                }
                preg_match("/\\xff(\\xda|\\xc2)/", $buffer, $matches);
                if (count($matches)) {
                    if (bin2hex($matches[0]) == 'ffda') {
                        $result = false;
                        break;
                    } else {
                        if (bin2hex($matches[0]) == 'ffc2') {
                            $result = true;
                            break;
                        }
                    }
                }
            } // end while
            fclose($fs);
        } // end if
        return $result;
    }

    /**
     * convertProgressiveJpg
     *
     * @param $path
     * Path - way to your images folder.
     * Function get image and convert it to progressive,
     * if image has not set as progressive yet;
     */
    public function convertProgressiveJpg($path)
    {
        $images = scandir($path, 1);
        $total = 0;
        foreach ($images as $image) {
            $ext = $this->getExt($image);
            if (in_array(strtolower($ext), array('jpeg', 'jpg'))) {
                $name = $this->getName($image);
                if (!$this->isProgressive($path . '/' . $image)) {
                    $tmpImg = imagecreatefromjpeg($path . '/' . $image);
                    imageinterlace($tmpImg, 1);
                    imagejpeg($tmpImg, $path . '/' . $image, 100);
                    imagedestroy($tmpImg);
                    $total++;
                    echo $path . '/' . $name . '.' . $ext . ' has converted successful <br/>';
                }
            }
        }
        echo '-----------------------------------------------------------------------<br/>';
        echo 'total: ' . $total . ';';
    }
}

$path = __DIR__;
$instance = new JpgProgressiveConverter();
$instance->convertProgressiveJpg($path);
